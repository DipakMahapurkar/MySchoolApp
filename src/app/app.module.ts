import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule, ErrorHandler } from '@angular/core';

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { IonicStorageModule } from '@ionic/storage';
import { PdfViewerComponent } from 'ng2-pdf-viewer';

import { ConferenceApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { PopoverPage } from '../pages/about-popover/about-popover';
import { AccountPage } from '../pages/account/account';
import { MapPage } from '../pages/map/map';
import { SchedulePage } from '../pages/schedule/schedule';
import { ScheduleFilterPage } from '../pages/schedule-filter/schedule-filter';
import { SessionDetailPage } from '../pages/session-detail/session-detail';
import { SignupPage } from '../pages/signup/signup';
import { SpeakerDetailPage } from '../pages/speaker-detail/speaker-detail';
import { SpeakerListPage } from '../pages/speaker-list/speaker-list';
import { TabsPage } from '../pages/tabs-page/tabs-page';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { SupportPage } from '../pages/support/support';

import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';

import { HomePage } from '../pages/home/home';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { LoginPage } from '../pages/login/login';
import { AttendancePage } from '../pages/attendance/attendance';
import { MonthlyReportPage } from '../pages/monthly-report/monthly-report';
import { ApplicationPage } from '../pages/application/application';
import { FeeReminderPage } from '../pages/fee-reminder/fee-reminder';
import { HomeworkPage } from '../pages/homework/homework';
import { MarksheetPage } from '../pages/marksheet/marksheet';
import { NoticePage } from '../pages/notice/notice';
import { TimelinePage } from '../pages/timeline/timeline';
import { TimetablePage } from '../pages/timetable/timetable';
import { YearlySchedulePage } from '../pages/yearly-schedule/yearly-schedule';

import { AddTimelinePage } from '../pages/add-timeline/add-timeline';


import { RestapiServiceProvider } from '../providers/restapi-service/restapi-service';
import { PassDataServiceProvider } from '../providers/pass-data-service/pass-data-service';
import { LoadingProvider } from '../providers/loading/loading';
import { CommonAlertProvider } from '../providers/common-alert/common-alert';


@NgModule({
    declarations: [
        ConferenceApp,
        AboutPage,
        AccountPage,
        LoginPage,
        MapPage,
        PopoverPage,
        SchedulePage,
        ScheduleFilterPage,
        SessionDetailPage,
        SignupPage,
        SpeakerDetailPage,
        SpeakerListPage,
        TabsPage,
        TutorialPage,
        SupportPage,

        PdfViewerComponent,

        HomePage,
        ChangePasswordPage,
        AttendancePage,
        MonthlyReportPage,
        ApplicationPage,
        FeeReminderPage,
        HomeworkPage,
        MarksheetPage,
        NoticePage,
        TimelinePage,
        TimetablePage,
        YearlySchedulePage,
        AddTimelinePage,
    ],
    imports: [
        BrowserModule,
        HttpModule,
        IonicModule.forRoot(ConferenceApp, {}, {
            links: [
                { component: TabsPage, name: 'TabsPage', segment: 'tabs-page' },
                { component: SchedulePage, name: 'Schedule', segment: 'schedule' },
                { component: SessionDetailPage, name: 'SessionDetail', segment: 'sessionDetail/:sessionId' },
                { component: ScheduleFilterPage, name: 'ScheduleFilter', segment: 'scheduleFilter' },
                { component: SpeakerListPage, name: 'SpeakerList', segment: 'speakerList' },
                { component: SpeakerDetailPage, name: 'SpeakerDetail', segment: 'speakerDetail/:speakerId' },
                { component: MapPage, name: 'Map', segment: 'map' },
                { component: AboutPage, name: 'About', segment: 'about' },
                { component: TutorialPage, name: 'Tutorial', segment: 'tutorial' },
                { component: SupportPage, name: 'SupportPage', segment: 'support' },
                { component: LoginPage, name: 'LoginPage', segment: 'login' },
                { component: AccountPage, name: 'AccountPage', segment: 'account' },
                { component: SignupPage, name: 'SignupPage', segment: 'signup' }
            ]
        }),
        IonicStorageModule.forRoot({
            name: 'SchoolApp',
            driverOrder: ['websql', 'indexeddb', 'sqlite']
        }),
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        ConferenceApp,
        AboutPage,
        AccountPage,
        LoginPage,
        MapPage,
        PopoverPage,
        SchedulePage,
        ScheduleFilterPage,
        SessionDetailPage,
        SignupPage,
        SpeakerDetailPage,
        SpeakerListPage,
        TabsPage,
        TutorialPage,
        SupportPage,

        HomePage,
        ChangePasswordPage,
        AttendancePage,
        MonthlyReportPage,
        ApplicationPage,
        FeeReminderPage,
        HomeworkPage,
        MarksheetPage,
        NoticePage,
        TimelinePage,
        TimetablePage,
        YearlySchedulePage,
        AddTimelinePage,
    ],
    providers: [
        { provide: ErrorHandler, useClass: IonicErrorHandler },
        ConferenceData,
        UserData,
        InAppBrowser,
        SplashScreen,
        StatusBar,

        RestapiServiceProvider,
        PassDataServiceProvider,
        LoadingProvider,
        CommonAlertProvider
    ]
})
export class AppModule { }
