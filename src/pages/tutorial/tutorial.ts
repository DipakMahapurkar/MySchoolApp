import { Component, ViewChild } from '@angular/core';

import { MenuController, NavController, Slides } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { TabsPage } from '../tabs-page/tabs-page';
import { LoginPage } from '../login/login';

import { PassDataServiceProvider } from '../../providers/pass-data-service/pass-data-service';


@Component({
    selector: 'page-tutorial',
    templateUrl: 'tutorial.html'
})

export class TutorialPage {
    showSkip = true;

    @ViewChild('slides') slides: Slides;

    constructor(
        public navCtrl: NavController,
        public menu: MenuController,
        public storage: Storage,
        private passDataServiceProvider: PassDataServiceProvider,

    ) { }

    startApp() {
        this.navCtrl.push(LoginPage).then(() => {
            this.passDataServiceProvider.setData('true', 'hasSeenTutorial');
        })
    }

    onSlideChangeStart(slider: Slides) {
        this.showSkip = !slider.isEnd();
    }

    ionViewWillEnter() {
        this.slides.update();
    }

    ionViewDidEnter() {
        // the root left menu should be disabled on the tutorial page
        this.menu.enable(false);
    }

    ionViewDidLeave() {
        // enable the root left menu when leaving the tutorial page
        this.menu.enable(true);
    }

}
